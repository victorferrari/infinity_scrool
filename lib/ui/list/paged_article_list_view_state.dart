import 'package:readwenderlich/entities/article.dart';

class PagedArticleListViewState {
  PagedArticleListViewState({
    this.itemList,
    this.error,
    this.nextPageKey = 0,
  });

  final List<Article> itemList;
  final dynamic error;
  final int nextPageKey;
}