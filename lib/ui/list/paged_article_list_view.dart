import 'dart:async';

import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:readwenderlich/data/repository.dart';
import 'package:readwenderlich/entities/article.dart';
import 'package:readwenderlich/ui/exception_indicators/empty_list_indicator.dart';
import 'package:readwenderlich/ui/exception_indicators/error_indicator.dart';
import 'package:readwenderlich/ui/list/article_list_item.dart';
import 'package:readwenderlich/ui/list/paged_article_list_view_bloc.dart';
import 'package:readwenderlich/ui/list/search_params.dart';
import 'package:readwenderlich/ui/preferences/list_preferences.dart';

class PagedArticleListView extends StatefulWidget {
  PagedArticleListView({
    @required this.repository,
    this.listPreferences,
    Key key,
  })  : assert(repository != null),
        super(key: key);

  final Repository repository;
  final ListPreferences listPreferences;

  @override
  _PagedArticleListViewState createState() => _PagedArticleListViewState();
}

class _PagedArticleListViewState extends State<PagedArticleListView> {
  ListPreferences get _listPreferences => widget.listPreferences;
  final _pagingController = PagingController<int, Article>(firstPageKey: 1);
  StreamSubscription _blocListingStateSubscription;
  PagedArticleListViewBloc _bloc;

  @override
  void initState() {
    _bloc = PagedArticleListViewBloc(repository: widget.repository);
    _pagingController.addPageRequestListener(
      (pageKey) {
        _bloc.onPageRequestSink.add(
          SearchParams(
            pageKey: pageKey,
            listPreferences: _listPreferences,
          ),
        );
      },
    );
    _blocListingStateSubscription = _bloc.onNewListingState.listen(
      (listingState) {
        _pagingController.value = PagingState(
          nextPageKey: listingState.nextPageKey,
          error: listingState.error,
          itemList: listingState.itemList,
        );
      },
    );
    super.initState();
  }

  @override
  void didUpdateWidget(PagedArticleListView oldWidget) {
    if (oldWidget.listPreferences != _listPreferences) {
      _pagingController.refresh();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _pagingController.dispose();
    _blocListingStateSubscription.cancel();
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => RefreshIndicator(
        onRefresh: () => Future.sync(
          () => _pagingController.refresh(),
        ),
        child: PagedListView.separated(
          pagingController: _pagingController,
          padding: const EdgeInsets.all(16),
          separatorBuilder: (context, index) => const SizedBox(
            height: 16,
          ),
          builderDelegate: PagedChildBuilderDelegate<Article>(
            itemBuilder: (context, article, index) => ArticleListItem(
              article: article,
            ),
            firstPageErrorIndicatorBuilder: (context) => ErrorIndicator(
              error: _pagingController.error,
              onTryAgain: () => _pagingController.refresh(),
            ),
            noItemsFoundIndicatorBuilder: (context) => EmptyListIndicator(),
          ),
        ),
      );
}
