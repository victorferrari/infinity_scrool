import 'package:flutter/widgets.dart';
import 'package:readwenderlich/ui/preferences/list_preferences.dart';

class SearchParams {
  SearchParams({
    @required this.pageKey,
    this.listPreferences,
  }) : assert(pageKey != null);

  final int pageKey;
  final ListPreferences listPreferences;
}
