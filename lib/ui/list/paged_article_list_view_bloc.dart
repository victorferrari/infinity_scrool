import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:readwenderlich/data/repository.dart';
import 'package:readwenderlich/entities/article.dart';
import 'package:readwenderlich/entities/list_page.dart';
import 'package:readwenderlich/ui/list/paged_article_list_view_state.dart';
import 'package:readwenderlich/ui/list/search_params.dart';
import 'package:rxdart/rxdart.dart';

class PagedArticleListViewBloc {
  PagedArticleListViewBloc({@required this.repository})
      : assert(repository != null) {
    _onPageRequest.stream
        .flatMap(_fetchArticleList)
        .listen(_onNewListingStateController.add)
        .addTo(_subscriptions);
  }

  static const _pageSize = 15;
  final Repository repository;
  final _subscriptions = CompositeSubscription();

  final _onNewListingStateController =
      BehaviorSubject<PagedArticleListViewState>.seeded(
    PagedArticleListViewState(),
  );
  final _onPageRequest = StreamController<SearchParams>();

  Stream<PagedArticleListViewState> get onNewListingState =>
      _onNewListingStateController.stream;

  Sink<SearchParams> get onPageRequestSink => _onPageRequest.sink;

  Stream<PagedArticleListViewState> _fetchArticleList(
      SearchParams params) async* {
    final lastListingState = _onNewListingStateController.value;
    try {
      final ListPage<Article> newItems = await repository.getArticleListPage(
        number: params.pageKey,
        size: _pageSize,
        filteredPlatformIds: params.listPreferences?.filteredPlatformIds,
        filteredDifficulties: params.listPreferences?.filteredDifficulties,
        filteredCategoryIds: params.listPreferences?.filteredCategoryIds,
        sortMethod: params.listPreferences?.sortMethod,
      );
      final isLastPage = newItems.itemList.length < _pageSize;
      final nextPageKey = isLastPage ? null : params.pageKey + 1;
      yield PagedArticleListViewState(
        error: null,
        nextPageKey: nextPageKey,
        itemList: params.pageKey != 1
            ? [...lastListingState.itemList, ...newItems.itemList]
            : newItems.itemList,
      );
    } catch (e) {
      yield PagedArticleListViewState(
        error: e,
        nextPageKey: lastListingState.nextPageKey,
        itemList: lastListingState.itemList,
      );
    }
  }

  void dispose() {
    _onNewListingStateController.close();
    _subscriptions.dispose();
    _onPageRequest.close();
  }
}
